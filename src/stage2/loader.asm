section .stage2
bits 32

global load_stage2
load_stage2:
  sti
  mov ebp, 0x0007000
  mov esp, 0x0008000
  extern stage2
  call stage2
  cli
  hlt