#include <stage2/ata.h>
#include <stdint.h>
#include <common/def.h>

enum ATA_Error {
  AMNF  = 1 << 0,      /* Address Mark Not Found    */
  TKZNF = 1 << 1,      /* TracK Zero Not Found      */
  ABRT  = 1 << 2,      /* ABoRTed command           */
  MCR   = 1 << 3,      /* Media Change Request      */
  IDNF  = 1 << 4,      /* ID Not Found              */
  MC    = 1 << 5,      /* Media Change              */
  UNC   = 1 << 6,      /* UNCorrectable data error  */
  BBK   = 1 << 7,      /* Bad BlocK detected        */
};

enum ATA_Status {
  ERR   = 1 << 0,      /* Error has occurred        */
  IDX   = 1 << 1,      /* Index (always 0)          */
  CORR  = 1 << 2,      /* Corrected data (always 0) */
  DRQ   = 1 << 3,      /* Either has data PIO ready
                        * or is read for PIO data   */
  SRV   = 1 << 4,      /* overlapped service mode    
                        * request                   */
  DF    = 1 << 5,      /* Drive fault (ERR unset)   */
  RDY   = 1 << 6,      /* Clear when drive is spun  
                          down or after error       */
  BSY   = 1 << 7,      /* Preparing to send/receive */
};


union {
  pstruct {
    uint8_t err   :1;
    uint8_t idx   :1;
    uint8_t corr  :1;
    uint8_t drq   :1;
    uint8_t srv   :1;
    uint8_t df    :1;
    uint8_t rdy   :1;
    uint8_t bsy   :1;
  };

  uint8_t raw;
} status_reg;
