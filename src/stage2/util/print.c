#include <stage2/print.h>

#define VGA_HEIGHT (25)
#define VGA_WIDTH (80)
#define VGA_SIZE ((VGA_WIDTH*VGA_HEIGHT))

static uint16_t* vga_ptr = (uint16_t*)0xb8000;
static uint32_t vga_index = 0;

static inline uint16_t make_color(enum color bg, enum color fg, uint8_t ch) {
  return ((bg << 12) | (fg << 8)) | ch;
}


struct {
  enum color bg;
  enum color fg;
} screen_attr = { .fg = CYAN, .bg = BLACK };

void inline scrollback() {
  vga_index = VGA_SIZE - VGA_WIDTH;
  for(uint32_t i = 0; i < VGA_SIZE-VGA_WIDTH; i++) {
    vga_ptr[i] = vga_ptr[i + VGA_WIDTH];
  }

  for(uint32_t i = 0; i < VGA_WIDTH; i++) {
    vga_ptr[vga_index+i] = 0x0000;
  }
}

void putchar(uint8_t ch) {
  if(vga_index >= VGA_SIZE) {
    scrollback();
  }
  vga_ptr[vga_index++] = make_color(screen_attr.bg, screen_attr.fg, ch);
}

static void inline print_u4(uint8_t nybble) {
  static const char* const hexstr = "0123456789ABCDEF";
  putchar(hexstr[(nybble & 0xF)]);
}

void print_u32(uint32_t dword) {
  print_u16(dword >> 16);
  print_u16(dword & 0xFFFF);
}

void print_u16(uint16_t word) {
  print_u8(word >> 8);
  print_u8(word & 0xFF);
}
void print_u8(uint8_t byte) {
  print_u4(byte>>4);
  print_u4(byte & 0x0F);
}

/* simple auxiliary print function, only used before userspace is loaded, this may be tweaked to write
 * to a log file later on
 */
void print(const char* str) {
  if(!str) return;

  while(*str != '\0') {
    if(vga_index >= VGA_SIZE) {
      scrollback();
    }

    /* Quick note:
     * I decided that \n would both go to the beginning of the line AND go to the next line 
     * (like \r\n in Windows, or just \n in Unix)
     */
    switch(*str) {
      case '\n':
        vga_index += VGA_WIDTH;
      break;
      case '\r':
        vga_index -= vga_index % VGA_WIDTH;
      break;
      default:
        vga_ptr[vga_index++] = make_color(screen_attr.bg, screen_attr.fg, *str);
      break;
    }

    str++;
  }
}
