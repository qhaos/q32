; to ignore warnings with resb and zeroing
%macro zerob 1
  times %1 db 0
%endmacro

[BITS 16]
org 0x7c00

KERNEL_LOAD equ 0x1000

jmp 0x00:boot

boot:
  mov [disk], dl
  
  ; Get to 03h VGA mode
  mov ax, 0x0003
  int 0x10

  xor ax, ax
  mov es, ax
  mov ds, ax
  mov ss, ax
  mov bp, 0x7C00
  mov sp, bp
  xor di, di

; read in superblock
  mov ax, 0x0201
  mov cx, 0x0003
  mov dh, 0x00
  mov dl, [disk]
  mov bx, superblock
  int 0x13

  mov ax, 2
  xor dx, dx
  mov di, 4
  mov bx, block_group_descriptor_table
  call read_blocks

  ; root is inode 2
  mov ax, 2
  call read_inode

  ; check the first block pointer
  mov ax, [inode.dbptr0]
  xor dx, dx
  mov bx, dirent
  call read_blocks

  ; find the block of the file
  mov si, dirent
  mov di, stage2_name
  mov cx, stage2_name_len
  call find_file

  ; inode is in ax
  call print_hex

  ; now that we know the block, read it's inode and then print it out to console
  call read_inode

  mov si, inode.dbptr0

  mov cx, [superblock.blocksize]
  mov di, 1024
  shl di, cl
  mov [adjusted_blksize], di
  mov cx, 12
  mov di, 1
  mov bx, KERNEL_LOAD
  xor dx, dx


; load kernel
  .loadloop:
  lodsw
  call print_hex
  or al, al
  jz .endload
  add si, 2
  call read_blocks
  add bx, [adjusted_blksize]
  loop .loadloop
  .endload:


  ; switch to mode Text Mode 03h
  mov ax, 0x0003
  int 0x10

  ; ======= get to 32-bit mode =======
  ; 1. trip A20
  ; 2. Load GDT, flat is fine for now

  mov ax, 0x2401
  int 0x15

  cli
  lgdt [gdt_ptr]
  mov eax, cr0
  or eax, 0x01
  mov cr0, eax

  jmp 8:KERNEL_LOAD
 
gdt_ptr:
  dw 24
  dd gdt

gdt:
  .null:
    dq 0
  .code:
  dw 0xFFFF
  dw 0x0000
  db 0x0000
  db 0b10011110
  db 0b1100_1111
  db 0x00
  .dat:
  dw 0xFFFF
  dw 0x0000
  db 0x00
  db 0b10010110
  db 0b1100_1111
  db 0x00

; +------------------------------------------------------------------+
; |  ___        __        __   __           __    _    __   ___  _   |
; | |___  |\ | |  \      /  \ |__    |     /  \  /_\  |  \  |__ |_|  |
; | |___  | \| |__/      \__/ |      |___  \__/ /   \ |__/  |__ |\   |
; +------------------------------------------------------------------+

; find_file(si :*dirent, di :str, cx :u16) -> ax: block | 0
find_file:
  push bx
  .loop:
  ; load inode into ax
  mov ax, [si]
  call print_hex
  or ax, ax
  jz .end
  mov bx, [si+4]
  xor ch, ch
  mov cl, [si+6]
  add bx, si
  add si, 8
  call print_buff
  push di
  push si
  call strncmp
  pop si
  pop di
  je .end
  mov si, bx
  jmp .loop
  .end:
  pop bx
  ret


; read_inode(ax :inode) -> void
read_inode:
  pusha

  xor dx, dx
  dec ax
  mov bx, [superblock.num_inodes_in_group]
  div bx
  ; index is dx, block group is ax
  shl ax, 5
  mov bx, ax
  add bx, block_group_descriptor_table
  ; grab lba of inode table
  mov ax, [bx+8]

  ; this part is a bit hairy:
  ; basically, I only want to read in one block
  ; so what I do is take my index (in bx for now)
  ; and then multiply by 128 and divide by the blocksize
  ; but we still need to save the remainder and use that to offset the target
  ; pointer so that it still gets read neatly into the global variable `inode`


  ; for example, say the blocksize is 1024
  ; bx * 128 / 1024 = bx / 8 = bx >> 3 remainder bx & 0b00000111

  ; keep in mind that blocksize is stored as 1024 << blocksize
  mov cl, [superblock.blocksize]
  add cl, 3

  mov bx, dx
  shr dx, cl
  mov di, 1
  shl di, cl
  dec di
  and bx, di
  shl bx, 7
  neg bx
  add bx, inode
  add ax, dx

  xor dx, dx
  mov di, 1
  call read_blocks

  popa
  ret


; read_blocks(ax :blockno, di :count, es:bx :pointer) -> void
read_blocks:
  pusha
  mov cl, [superblock.blocksize]
  inc cl
  shl ax, cl
  shl di, cl
  mov byte [disk_address_packet.sizeof], 0x10
  mov [disk_address_packet.lba], ax
  mov [disk_address_packet.sectors], di
  mov [disk_address_packet.dst_ptr+2], es
  mov [disk_address_packet.dst_ptr], bx
  mov ax, 0x4200
  mov si, disk_address_packet
  mov dl, [disk]
  int 0x13

  popa
  ret

; char in al
putchar:
  push ax
    mov ah, 0x0E
    int 0x10
  pop ax
  ret

; print_hex(ax :u16) -> void, clobber: none
print_hex:
  pusha
  mov dx, ax
  mov ah, 0x0E
  mov cx, 4
  .loop:
    mov al, dh
    shr al, 4
    cmp al, 10
    jl .hex_digit
    ; if it's not 0-9, we can't just add 0x30
    add al, ('A'-'0'-10)
    .hex_digit:
    add al, '0'
    shl dx, 4
    int 0x10
  loop .loop
  mov al, ' '
  call putchar
  popa
  ret

; buff in es:si, count in cx
print_buff:
  push si
  push ax
  push cx
  mov ah, 0x0E
  .loop:
    lodsb
    int 0x10
  loop .loop
  mov al, 13
  call putchar
  mov al, 10
  call putchar
  pop cx
  pop ax
  pop si
  ret

; strncmp(ds:si :str, es:di :str, cx :u16) -> flags
strncmp:
  push cx
  .loop:
    cmpsb
    jne .exit
  loop .loop
  .exit:
  pop cx
  ret
 
stage2_name: db 'stage2.bin'
stage2_name_len equ $-stage2_name
zerob 510-($-$$)
dw 0xAA55

adjusted_blksize: zerob 2
disk: zerob 1
disk_address_packet:
  .sizeof:  zerob 1   ; always 0x10
  .unused:  zerob 1   ; always 0x00
  .sectors: zerob 2
  .dst_ptr: zerob 4
  .lba:     zerob 8   ; starts at zero

; this is where we will load the superblock
%include "include/boot/asm/superblock.asm"
zerob 1024+512-($-$$)
%include "include/boot/asm/blockgrouptable.asm"
zerob 4096
; we only need to load in inode 2 for now, so we will parse it right here:
; we want inode 2, so skipping 128 bytes
zerob 1024
%include "include/boot/asm/inode.asm"
zerob 1024
dirent: