block_group_descriptor_table:
  .block_usage_bitmap_start:  zerob 4
  .inode_usage_bitmap_start:  zerob 4
  .inode_table_start:         zerob 4
  .num_unalloc_blocks:        zerob 2
  .num_directories:           zerob 2