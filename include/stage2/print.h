#pragma once
#include <stdint.h>

/* print functions temporarily, until the real kernel is loaded that takes care of I/O */

void print(const char*);
void print_u32(uint32_t);
void print_u16(uint16_t);
void print_u8(uint8_t);
void putchar(uint8_t);


enum color {
  BLACK,
  BLUE,
  GREEN,
  CYAN,
  RED,
  MAGENTA,
  BROWN,
  LIGHT_GRAY,
  DARK_GRAY,
  LIGHT_BLUE,
  LIGHT_GREEN,
  LIGHT_CYAN,
  LIGHT_RED,
  LIGHT_MAGENTA,
  YELLOW,
  WHITE
};
