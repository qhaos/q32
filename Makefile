CC :=clang --target=i686-pc-none-elf
CFLAGS := -Iinclude -m32 -ffreestanding -fno-builtin -nostdlib -O2 -Wall
BOOTDIR := src/boot
STAGE2DIR := src/stage2
OBJDIR := obj

all: stage2 boot fs


stage2: $(STAGE2DIR)/loader.asm $(STAGE2DIR)/stage2.c
	mkdir -p $(OBJDIR)/stage2
	nasm -felf32 $(STAGE2DIR)/loader.asm -o $(OBJDIR)/stage2/loader.o
	$(CC) $(CFLAGS) -c $(STAGE2DIR)/stage2.c -o $(OBJDIR)/stage2/stage2.o
	$(CC) $(CFLAGS) -c $(STAGE2DIR)/util/*.c
	mv *.o $(OBJDIR)/stage2
	$(CC) $(CFLAGS) -T build/stage2.ld -o mnt/stage2.bin $(OBJDIR)/stage2/*.o -lgcc

boot: $(BOOTDIR)/boot.asm
	nasm $(BOOTDIR)/boot.asm -o $(OBJDIR)/boot.bin

fs:
	dd if=/dev/zero of=disk.img bs=512 count=5120
	mkfs.ext2 disk.img -d mnt
	dd if=$(OBJDIR)/boot.bin of=disk.img bs=512 count=1 conv=notrunc

run: disk.img
	qemu-system-i386 disk.img

clean:
	rm -r $(OBJDIR)/*
